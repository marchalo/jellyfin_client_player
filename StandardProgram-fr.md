# Standard de programmation Javascript

- La taille maximale d'une ligne est de 80 caractères
- Chaque indentation est de 4 espaces
- Utilisez des points-virgules
- Utilisez des guillemets simples
- Utilisez const si la variable ne change pas
- Utilisez let si la variable change
- Utilisez l'opérateur d'égalité triple


- Les variables, les propriétés et les noms de fonction doivent utiliser "lowerCamelCase". Ils doivent également être descriptifs. Les variables à caractère unique et les abréviations peu courantes doivent généralement être évitées

      const adminUser = db.query(‘SELECT * FROM users …’);

- Les noms de classe doivent être en majuscules en utilisant "UpperCamelCase"

      function BankAccount() {}

- Les constantes doivent être déclarées en tant que variables régulières ou propriétés de classe statiques, en utilisant toutes les lettres majuscules

      const SECOND = 1 * 1000;
      function File() {
      }

      File.FULL_PERMISSIONS = 0777;

- Utilisez des virgules de fin et mettez des déclarations courtes sur une seule ligne. Ne citez les clés que lorsque votre interprète se plaint

      let a = ['hello', 'world'];

      let b = {
        good: 'code',
        'is generally': 'pretty',
      };

- Toutes les conditions non triviales doivent être affectées à une variable ou à une fonction nommée de manière descriptive

      let isValidPassword = password.length >= 4 && /^(?=.*\d).{4,}$/.test(password);

      if (isValidPassword) {
        console.log('winning');
      }

- Gardez vos fonctions courtes. Une bonne fonction tient sur une diapositive que les personnes au dernier rang d'une grande salle peuvent lire confortablement

- Renvoyez une valeur d'une fonction le plus tôt possible est acceptée, mais prioriser une seule valeur de retour

- Une méthode par ligne doit être utilisée si vous souhaitez chaîner des méthodes

      User
      .findOne({ name: ‘foo’ })
      .populate(‘bar’)
      .exec(function(err, user) {
      return true;
      });


- Utilisez des barres obliques pour les commentaires sur une seule ligne et sur plusieurs lignes. Essayez d'écrire des commentaires qui expliquent les mécanismes de niveau supérieur ou clarifient les segments difficiles de votre code. N'utilisez pas de commentaires pour reformuler des choses triviales


      // 'ID_SOMETHING=VALUE' -> ['ID_SOMETHING=VALUE', 
      // 'SOMETHING', 'VALUE']
      let matches = item.match(/ID_([^\n]+)=([^\n]+)/));

      // This function has a nasty side effect where a failure to 
      // increment a redis counter used for statistics will 
      // cause an exception. This needs to be fixed in a later iteration.
      function loadUser(id, cb) {
        // ...
      }

      let isSessionValid = (session.expires < Date.now());
      if (isSessionValid) {
        // ...
      }


- Mettez toujours les exigences en haut du fichier pour illustrer clairement les dépendances d'un fichier. En plus de donner un aperçu aux autres en un coup d'œil rapide des dépendances et de l'impact possible sur la mémoire, cela permet de déterminer s'ils ont besoin d'un fichier package.json s'ils choisissent d'utiliser le fichier ailleurs

- Toutes les structures de contrôles (routines, if, for, while, etc.) doivent être délimitées par des
accolades ('{' et '}')

- Tous les mots-clés et opérateurs (if, for, +, =) doivent être entourés de caractère d'espacement