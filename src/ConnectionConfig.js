import {AppConfig} from "./AppConfig.js";

export class ConnectionConfig extends AppConfig {
    address;
    name;
    version;
    id;
    capabilities;
    constructor() {
        super();
        this.address = 'http://localhost:8096';
        this.name = 'pie';
        this.version = '0.0.1';
        this.id = 'WebNg';
        this.capabilities = 'WebNg';
    }
    // methode


}