import 'node-self';
import fetch from 'node-fetch'
globalThis.fetch = fetch
import JellyfinApiClient from "jellyfin-apiclient";
import {ConnectionConfig} from "./ConnectionConfig.js"


export class ConnectionManage {
    connectionConfig;
    apiClient;
    constructor() {
        this.connectionConfig = new ConnectionConfig();
        this.apiClient = new JellyfinApiClient.ApiClient('http://192.168.0.11:8096',
        'Terminal Client',
        '0.0.1',
        'Headless Client',
        'TW96aWxsYS81LjAgKFgxMTsgTGludXggeDg2XzY0OyBydjo3NC4wKSBHZWNrby8yMDEwMDEwMSBGaXJlZm94Lzc0LjB8MTU4NDkwMTA5OTY3NQ11');
    }
    // methode
    async connection() {
      let authentification = await this.apiClient.authenticateUserByName("Username", "password");
      console.log(JSON.stringify(authentification, null, 2));
      this.apiClient.setAuthenticationInfo(authentification.AccessToken, authentification.User.Id);
      console.log(this.apiClient.isLoggedIn());
      let artists = await this.apiClient.getArtists(authentification.User.Id);
      console.log(JSON.stringify(artists, null, 2));
    }

}